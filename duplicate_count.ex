defmodule DuplicateCount do
  def count(str) do
    str
    |> String.downcase()
    |> String.split("", trim: true)
    |> Enum.map_reduce(
         %{},
         fn s, map ->
           case Map.fetch(map, s) do
             {:ok, count} -> {[s], Map.put(map, s, count + 1)}
             :error -> {[s], Map.put(map, s, 1)}
           end
         end
       )
    |> elem(1)
    |> Enum.filter(fn {_, count} -> count > 1 end)
    |> length()
  end
end

count = &DuplicateCount.count/1
IO.inspect(count.("")) #0
IO.inspect(count.("abcde")) #0
IO.inspect(count.("aabbcde")) #2
IO.inspect(count.("aabBcde")) #2
IO.inspect(count.("Indivisibility")) #1
IO.inspect(count.("Indivisibilities")) #2